## Introduction

This module contains allows cookies to be encrypted, the client can't read the name or the value of
the cookie in plain text.

## Requirements

 * SilverStripe 4.0
 * Illuminate Encryption 8.0