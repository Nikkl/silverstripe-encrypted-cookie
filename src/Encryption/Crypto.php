<?php


namespace Nikkl\Encryption;


use Illuminate\Encryption\Encrypter;
use SilverStripe\Core\Environment;

class Crypto
{
    /**
     * @var ?Encrypter
     */
    protected static ?Encrypter $encrypter = null;

    /**
     * @return Encrypter
     */
    protected static function encrypter(): Encrypter
    {
        $key = Environment::getEnv('SS_ENCRYPTION_KEY');

        if (!isset(self::$encrypter)) {
            self::$encrypter = new Encrypter(md5($key ?? __DIR__), 'AES-256-CBC');
        }

        return self::$encrypter;
    }

    /**
     * @param  string  $key
     * @param  string  $cipher
     * @return bool
     */
    public static function supported(string $key, string $cipher): bool
    {
        return Encrypter::supported($key, $cipher);
    }

    /**
     * @param  string  $cipher
     * @return string
     */
    public static function generateKey(string $cipher): string
    {
        return Encrypter::generateKey($cipher);
    }

    /**
     * @param mixed $value
     * @param bool $serialize
     * @return string
     */
    public static function encrypt($value, bool $serialize = true): string
    {
        return self::encrypter()->encrypt($value, $serialize);
    }

    /**
     * @param string $value
     * @return string
     */
    public static function encryptString(string $value): string
    {
        return self::encrypter()->encryptString($value);
    }

    /**
     * @param string $payload
     * @param bool $unserialize
     * @return mixed
     */
    public static function decrypt(string $payload, bool $unserialize = true)
    {
        return self::encrypter()->decrypt($payload, $unserialize);
    }

    /**
     * @param string $value
     * @return string
     */
    public static function decryptString(string $value): string
    {
        return self::encrypter()->decryptString($value);
    }
}
