<?php


namespace Nikkl\Control;


use App\Encryption\Crypto;
use Illuminate\Contracts\Encryption\DecryptException;
use SilverStripe\Control\Cookie;

/**
 * Class SecureCookie
 * @package App\Extension
 */
class SecureCookie extends Cookie
{
    /**
     * @param string $name
     * @param mixed $value
     * @param int $expiry
     * @param null $path
     * @param null $domain
     * @param false $secure
     * @param bool $httpOnly
     * @return mixed
     */
    public static function set(
        $name,
        $value,
        $expiry = 90,
        $path = null,
        $domain = null,
        $secure = false,
        $httpOnly = true
    ) {
        return parent::set(
            md5($name),
            Crypto::encryptString($value),
            $expiry,
            $path,
            $domain,
            $secure,
            $httpOnly
        );
    }

    /**
     * @param string $name
     * @param bool $includeUnsent
     * @return string|null
     */
    public static function get($name, $includeUnsent = true): ?string
    {
        $encrypted = parent::get(md5($name), $includeUnsent);

        if (!$encrypted) {
            return null;
        }

        try {
            return Crypto::decryptString($encrypted);
        } catch (DecryptException $e) {
            return null;
        }
    }
}
